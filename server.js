const express = require('express');
const cors = require('cors');
const app = express();

app.use('/libraries', express.static(__dirname + '/node_modules'));
app.use('/assets', express.static(__dirname + '/assets'));
app.use('/js', express.static(__dirname + '/js'));
app.use(cors());

app.set('port', (process.env.PORT || 5001));

// Serve App
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.listen(app.get('port'), function() {
  console.log("localhost:" + app.get('port'));
});
