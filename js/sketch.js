/**
Game Logic Programming Path
    //TODO: Create a game engine
    //TODO: Apply game assets
Game UI Programming Path
    //TODO: Create game menu
Game Graphic Design Path
    //TODO: Create assets
*/

//variables
let nextScene = "";
let menuScene = "game"; // Scene that it starts on
let gameLevel = "prototype";

//key logic setup
let keys = [];
function keyPressed(){
  keys[keyCode] = true;
}
function keyReleased(){
  keys[keyCode] = false;
}

function preload(){
    menuBackgroundImage = loadImage('https://t0.gstatic.com/images?q=tbn:ANd9GcRbPilus2wjh6tOKHdvmS2nRvBi075QYxoOvdnbMc27jEcI-FXS');
}
function setup() {
    createCanvas(1280,720);
    frameRate(60);

}
              //MENU SCENES 
//Menu Buttons and more
function sceneButton(x, y, w, h, buttonText, buttonAction){
  /*
  button = createButton(buttonText);
  button.style('background-color', color(25, 23, 200));
  button.position(x, y);
  button.size(w,h);
  button.onclick = buttonAction;
  **/
 rect(x,y,w,h);

}
//Main menu screen
function drawMenuScene() {
    background(menuBackgroundImage);

    //button 1
    fill(100, 255, 94);
    sceneButton(150,50,300,100,"Play",function(){scene = "game";} );

    /**
    button = createButton("test button");
    button.style('background-color', color(25, 23, 200));
    button.position(500, 100);
    button.size(100,50);
    button.mouseClicked = function(){
      console.log("this button was clicked");
    };
    */
    //button 2
    fill(66, 245, 236);
    sceneButton(150,200,200,100,"Settings",function(){scene = "game";});

    //button 3
    fill(186, 0, 22);
    sceneButton(150,350,200,100,"Help",function(){scene = "game";});

    //button 4
    fill(25, 121, 224);
    sceneButton(1050, 600,160,50,"Donate, pls",function(){scene = "game";});

    //smiley face on menu
    strokeWeight(0);
    fill(201, 224, 25)
    ellipse(800,300,300,300);
    strokeWeight(4);
    fill(0,0,0);
    ellipse(800,350,150,150);
    fill(201, 224, 25);
    strokeWeight(0);
    rect(700, 260, 200, 100);
    fill(0,0,0);
    ellipse(900,350,50,50);
    ellipse(600,350,50,50);
    rect(850,300,100,10);
    rect(550,300,120,10);



}
//Draws Credits scene
function drawCreditsScene(){
    background(0,0,0);
    fill(255,255,255);
    textSize(45);
    text("Programmers:",25,50);
    text("Graphic Designers:",25,150);
    text("Game Designers:",25,250);
}
/////////////////////////////////////// Core Game Objects, Functions, and Scenes ////////////////////////////////////
{//condensed arrays

let allRockColliders = [];


}


//Draws Experimental Core Game
    function drawGameScene(){
      if(gameLevel === "prototype"){
        background(120, 120, 255);
        renderRock(500,Player.y + 200,50,50);
        Player.render();
      }
    }

    class userPlayer {
        
        speed = 5;  //pixelsPerframe  5   =  5ppf = 5*60 pixels per seconds at 60 fps

        render = () => {
          //Temporary drawing, change later
          fill(255,0,0);
          strokeWeight(2);
          stroke(0,0,0);
          rect(this.x, this.y, this.w, this.h, 2);

          if(keys[this.keybinds.up]){this.move("up");}
          if(keys[this.keybinds.down]){this.move("down")}
          if(keys[this.keybinds.left]){this.move("left")}
          if(keys[this.keybinds.right]){this.move("right")}
        }

        move = (direction) => {
          //later make this check for colliders
          console.log(this.y)
          if(direction === "up"){this.changePosition(0,-5)}
          if(direction === "down"){this.changePosition(0,+5)}
          if(direction === "left"){this.changePosition(-5,0)}
          if(direction === "right"){this.changePosition(+5,0)}
        }

        changePosition = (x,y) =>{
            if(typeof x != "undefined"){
              this.x += x;
            }
            if(typeof y != "undefined"){
              this.y += y;
            }        
        }
        setPosition = (x,y) =>{
          if(typeof x != "undefined"){
            this.x = x;
          }
          if(typeof y != "undefined"){
            this.y = y;
          }        
      }


        constructor(x, y, w, h, keybinds){
          this.x = x;
          this.y = y;
          this.w = w;
          this.h = h;
          this.keybinds = keybinds;
        }
    }
    function renderRock(x,y,w,h){
      let currentRock = new collider(x,y,w,h);
      fill(200,200,200);
      strokeWeight(2);
      stroke(0,0,0);
      rect(x,y,w,h)
      //later add an array of rocks and have the currentRock variable added to it
    }
    Player = new userPlayer(20,20,50,100,{
    up: 87,
    down: 83,
    left: 65,
    right: 68
    });
    //console.log(Player)

    //TODO: ADD FUNCTIONS THAT CREATE COLLIDERS AND STORE THEM IN SPECIFIC TYPE ARRAY OF COLLIDERS? 
    
  class collider {
      constructor(obj){
        this.initBounds(obj)
        this.collisionCondition = obj.type;
      }

      initBounds = ({ x, y, width, height }) => {
        this.bounds = {
          x,
          y,
          width,
          height,
          left: x,
          right: x + width,
          top: y,
          bottom: y + height
        }
      }

      showColliderOutline = () => {
        noFill();
        stroke(255,0,0);
        strokeWeight(5);
        rect(this.x,this.y,this.w,this.h);
      }

      getBounds = () => {
        return this.bounds
      }

      isTouchingRight = (collider) => {
          return (
              (
                this.bounds.left > collider.bounds.left &&
                this.bounds.left < collider.bounds.right
              ) &&
              (
                this.bounds.top > collider.bounds.top &&
                this.bounds.top < collider.bounds.bottom
              )
          )

      }

  }

function draw(){
  console.log("framerate: " + frameRate());
  overSceneButton = false;
  allRockColliders = []; //removes all currently stored rock colliders to remove stacking

  //make this into a switch later or no???
  //console.log(scene);
  if(menuScene==="menu"){
    drawMenuScene();
  }else if(menuScene==="credits"){
    drawCreditsScene();
  }else if(menuScene==="game"){
    drawGameScene();
  }

    //console.log(scene)

}

function mousePressed(){
  /**
    if(overSceneButton){
      scene = nextScene;
      console.log("went to "+nextScene);
      console.log(scene)
    }
  */
}
